# ITCodeMiscellanea

Some random tools for IT module testing

**InitWorkingArea.py** is a script to create a working area in Ph2_ACF and copy there the relevant files needed to run the middleware (CMSIT.xml, and the RD53 text files for each roc). Two xml files are created, one to be used for noise scans the second for scurve or threshold minimization. See help for usage, a name for the working are is to be provided

**InitTextFiles.py** is a script to reset the RD53 text files, it can reset all the values (i.e copy back the default files for each ROC), or select a set of text files from a previous run. See the help for usage.