#!/usr/bin/python

from copy import deepcopy
import sys
import os
from shutil import copyfile 
import argparse
import numpy as np
from glob import glob

parser = argparse.ArgumentParser(description='Reset single column values in RD53 files')
parser.add_argument('name', help='Name of the file to reset')
args = vars(parser.parse_args())

def generate_mask_efficiency():
        mask = np.zeros([192,400], dtype=np.int)
        injection_rows = range(7,192,7)
        injection_columns = range(130,264,5)
        injection_pixels = zip(injection_rows, injection_columns)
        for pixel_location in injection_pixels: 
                mask[pixel_location] = 1
        return mask
    
def generate_default_injen():
        mask = np.zeros([192,400], dtype=np.int)
        return mask
    
def generate_default_enable():
        mask = np.ones([192,400], dtype=np.int)
        return mask
    
def generate_default_hitbus():
        mask = np.zeros([192,400], dtype=np.int)
        mask[:,128:254] = 1 #WHY IS THIS 254 in the default?
        return mask
        
def generate_default_tdac():
        mask = np.zeros([192,400], dtype=np.int)
        mask[:,0:264] = 7
        mask[:,264:400] = 15
        return mask

def read_mask(textfile, targetmask): #targetmasks are ENABLE, HITBUS, INJEN, TDAC
        f2 = open(textfile)
        rd53settings = f2.read()
        f2.close()
        maskstart = rd53settings.find("PIXELCONFIGURATION")
      
        colpositions = []
        start = maskstart
        col_position = rd53settings.find("COL", start)

        while col_position > 0:
                colpositions.append(col_position)
                start = col_position + 3
                col_position = rd53settings.find("COL", start)
        
        colnums = []     
        colnums_int = []   
        for i, col_position in enumerate(colpositions):
                colend = rd53settings.find("\n", col_position)
                col_num = rd53settings[colend-3:colend]
                colnums.append(col_num)
                colnums_int.append(int(col_num))

        mask = np.zeros([192, np.size(colnums_int)], dtype=np.int)
        tmask = np.transpose(mask)
        for i, col_position in enumerate(colpositions):
                setting_start = rd53settings.find(targetmask, col_position)
                setting_end = rd53settings.find("\n", setting_start)
                truncation = 7
                setting_raw = rd53settings[(setting_start+truncation):setting_end]
                setting_list = list(map(int,setting_raw.split(',')))
                tmask[i,:] = setting_list
        mask = np.transpose(tmask)
        return mask
    



    
def write_mask(textfile, targetmask, generated_mask): #targetmasks are ENABLE, HITBUS, INJEN, TDAC
        f2 = open(textfile)
        rd53settings = f2.read()
        f2.close()
        maskstart = rd53settings.find("PIXELCONFIGURATION")
   
        colpositions = []
        start = maskstart
        col_position = rd53settings.find("COL", start)

        while col_position > 0:
                colpositions.append(col_position)
                start = col_position + 3
                col_position = rd53settings.find("COL", start)
       
        colnums = []     
        colnums_int = []   
        for i, col_position in enumerate(colpositions):
                colend = rd53settings.find("\n", col_position)
                col_num = rd53settings[colend-3:colend]
                colnums.append(col_num)
                colnums_int.append(int(col_num))

    
        tmask = np.transpose(generated_mask)
        for i, col_position in enumerate(colpositions):
                setting_start = rd53settings.find(targetmask, col_position)
                setting_end = rd53settings.find("\n", setting_start)
                truncation = 7
                #new_line = tmask[i,:]
                new_line_str = ','.join(str(x) for x in tmask[i,:])
                rd53settings2 = rd53settings[:(setting_start+truncation)] + new_line_str + rd53settings[setting_end:]
                rd53settings = rd53settings2
                
        f3 = open(textfile, 'w')
        f3.write(rd53settings)
        f3.close()



#miss a way to change single pixel values

#this function reset only the targetmask
def reset_mask(filename,targetmask):#targetmasks are ENABLE, HITBUS, INJEN, TDAC
    
    mask = np.zeros([192,400], dtype=np.int)    
    
    if (targetmask=="HITBUS"):
            mask = np.zeros([192,400], dtype=np.int)
            mask[:,128:254] = 1 #WHY IS THIS 254 in the default?    
    elif (targetmask=="TDAC"):
        mask = np.zeros([192,400], dtype=np.int)
        mask[:,0:264] = 7
        mask[:,264:400] = 15

    write_mask(filename,targetmask,mask)
    
def set_column_value(filename,targetmask,col,value):
    
    mask=read_mask(filename,targetmask)
    mask[:,col]=int(value)
    write_mask(filename,targetmask,mask)

def set_row_value(filename,targetmask,row,value):
    
    mask=read_mask(filename,targetmask)
    mask[row,:]=int(value)
    write_mask(filename,targetmask,mask)

def set_pix_value(filename,targetmask,row,col,value):
    
    mask=read_mask(filename,targetmask)
    mask[row,col]=int(value)
    write_mask(filename,targetmask,mask)
#def set_pixel_value(filename,targetmask,row,col,val):





input_files=str(args['name'])

#efficiency_mask = generate_mask_efficiency()
#reset_mask('CMSIT_RD53.txt', 'TDAC')
#set_pix_value('CMSIT_RD53.txt', 'ENABLE', 100,399,99999999999999)
#write_mask('CMSIT_RD53.txt', 'INJEN', efficiency_mask)
#write_mask('CMSIT_RD53.txt', 'INJEN', generate_default_injen())

#a = generate_default_hitbus()
#b = generate_default_tdac()


