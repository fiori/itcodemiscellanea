#!/usr/bin/python

from copy import deepcopy
import sys
import os
import time
from shutil import copyfile
from lxml import etree 
import argparse

parser = argparse.ArgumentParser(description='Create a working dir for Ph2_ACF')
parser.add_argument('name', help='The name of the directory to create (workingFolder_ + name)')
parser.add_argument('--nrocs', type=int, help='(Optional) Number of ROCs in the chip (default=4)',default=4)

args = vars(parser.parse_args())

def indent(root):
    elems=root.findall(".//RD53/Settings")
    for elem in elems:
            for att in elem.attrib:
                 if (elem.get(att).find("#$%")==-1): elem.set(att,elem.get(att)+"#$%")

    elems=root.findall(".//Global")
    for elem in elems:
            for att in elem.attrib:
                  if (elem.get(att).find("%$%")==-1): elem.set(att,elem.get(att)+"%$%")
           
def cloneRD53Nodes(root,nrocs):
    element=root.find(".//RD53")    
    element.attrib['configfile']="CMSIT_RD53_"+str(0)+".txt"
    element.attrib['Id']="4"

    for i in range(nrocs-1):
        element.getparent().insert(1+i,deepcopy(element))  
        element.attrib['configfile']="CMSIT_RD53_"+str(i+1)+".txt"
        element.attrib['Lane']=str(i+1)
        element.attrib['Id']=str(4+i+1) 

def cleanAndSave(tree,test_dir,suffix,rocs):
    xmlFile=test_dir + "CMSIT_" + str(rocs)+"ROCs_"+ suffix + ".xml"
    s=etree.tostring(tree.getroot(),pretty_print=True,xml_declaration=True,encoding='utf8')

    s1=s.replace("#$%\"", "\"\n\t\t   " )
    s2=s1.replace("%$%\"", "\"\n\t       " )#to give the right indentation to the final xml

    x=open(xmlFile,"w")
    x.write(s2)
    x.close

def forNoise(root):#specialize the config for noise scans
    par1 = root.findall("./Settings/Setting[@name='nEvents']")
    par1[0].text='10000000'
    
    par2 = root.findall("./Settings/Setting[@name='nEvtsBurst']")
    par2[0].text='10000'

    par3 = root.findall("./Settings/Setting[@name='nClkDelays']")
    par3[0].text='50'

    par4 = root.findall("./Settings/Setting[@name='nTRIGxEvent']")
    par4[0].text='1'

    par5 = root.findall("./Settings/Setting[@name='INJtype']")
    par5[0].text='0'


def forScurve(root):#specialize the config for noise scans
    par1 = root.findall("./Settings/Setting[@name='nEvents']")
    par1[0].text='100'
    
    par2 = root.findall("./Settings/Setting[@name='nEvtsBurst']")
    par2[0].text='100'

    par3 = root.findall("./Settings/Setting[@name='nClkDelays']")
    par3[0].text='50'

    par4 = root.findall("./Settings/Setting[@name='nTRIGxEvent']")
    par4[0].text='10'

    par5 = root.findall("./Settings/Setting[@name='INJtype']")
    par5[0].text='1'



#put control here about the arguments (optparse)
codedir = "/home/cms/Ph2ACF_3_9_8_SW_3_4_FW/Ph2_ACF/"

modname=str(args['name'])
test_dir = codedir + "workingFolder_" + modname + "/"

rocs=args['nrocs']

print("Creating test dir" + test_dir)

#create the working directory
if not os.path.exists(test_dir):
    os.makedirs(test_dir)

print("Copy RD53 files for the " + str(rocs) + " ROCs")

for i in range(rocs):
    tmpname=test_dir + "CMSIT_RD53_"+str(i)+".txt"
    copyfile(codedir+'settings/RD53Files/CMSIT_RD53.txt',tmpname)

#now take the xml, add the correct number of rocs and change some values

xmlFileTmp=codedir+"settings/CMSIT.xml"

tree = etree.parse(xmlFileTmp)
root = tree.getroot()

cloneRD53Nodes(root,rocs)

#now create the two flavors noise and scurve
forNoise(root)
indent(root)
cleanAndSave(tree,test_dir, "noise", rocs)

forScurve(root)
#indent(root)
cleanAndSave(tree,test_dir, "scurve", rocs)
