#!/usr/bin/python

from copy import deepcopy
import sys
import os
import time
from shutil import copyfile 
import argparse

parser = argparse.ArgumentParser(description='Reset the RD53 txt files (option -1), or take the files from a given run (option RunNum). To be run in the same directory where the files to change are')
parser.add_argument('-mode', type=int, default=-1,help='With the default (-1) resets the text files, with the run number, copies text files from some past run')
parser.add_argument('-nrocs', type=int, default=4,help='The number of ROCs in the module (default 4)')

args = vars(parser.parse_args())

mode=int(args['mode'])
rocs=int(args['nrocs'])

codedir = "/home/fiori/ITcode/Ph2_ACF/"

if (mode == -1):
    print("Reset RD53 files for the " + str(rocs) + " ROCs")
    for i in range(rocs):
        tmpname="CMSIT_RD53_"+str(i)+".txt"
        copyfile(codedir+'settings/RD53Files/CMSIT_RD53.txt',tmpname)

if (mode>=0):
    file_dir=codedir+"Results"
    for i in range(rocs):
        tmpname="CMSIT_RD53_"+str(i)+".txt"
        if (mode<10):
            copyfile(file_dir+"Run00000"+str(mode)+"CMSIT_RD53_"+str(i)+".txt"tmpname)
        else:
            copyfile(file_dir+"Run0000"+str(mode)+"CMSIT_RD53_"+str(i)+".txt"+tmpname)